<?php

/**
 * Simple custom text box.
 */
class content_box extends boxes_box {
  /**
   * Implementation of boxes_box::options_defaults().
   */
  public function options_defaults() {
    return array(
      'default_body' => array(
        'value' => '',
        'format' => filter_default_format(),
      ),
    );
  }

  /**
   * Implementation of boxes_box::options_form().
   */
  public function options_form(&$form_state) {
    $format = filter_format_load($this->options['default_body']['format']);

    if (filter_access($format)) {
      $form = array();
      $form['default_body'] = array(
        '#type' => 'text_format',
        '#base_type' => 'textarea',
        '#title' => t('Default body'),
        '#default_value' => variable_get('content_box_' . $this->delta, $this->options['default_body']['value']),
        '#rows' => 6,
        '#format' => $this->options['default_body']['format'] ? $this->options['default_body']['format'] : NULL,
        '#description' => t('The default content to show if the variable is not set.'),
      );
      return $form;
    }
  }

  /**
   * Save a box.
   */
  public function save() {
    if (empty($this->delta)) {
      throw new Exception(t('Cannot save box without a specified delta.'));
    }
    self::reset();
    $existing = boxes_box_load($this->delta);

    variable_set('content_box_' . $this->delta, $this->options['default_body']['value']);

    if ($existing && ($existing->export_type & EXPORT_IN_DATABASE)) {
      drupal_write_record('box', $this, array('delta'));
    }
    else {
      drupal_write_record('box', $this);
    }
    $this->new = FALSE;
    self::reset();
    module_exists('context') ? context_invalidate_cache() : NULL;
  }

  /**
   * Implementation of boxes_box::render().
   */
  public function render() {
    $content = '';
    $content = check_markup(variable_get('content_box_' . $this->delta, $this->options['default_body']['value']), $this->options['default_body']['format'], $langcode = '' /* TODO Set this variable. */, FALSE);
    $title = isset($this->title) ? check_plain($this->title) : NULL;
    return array(
      'delta' => $this->delta, // Crucial.
      'title' => $title,
      'subject' => $title,
      'content' => $content,
    );
  }
}
